# OpenML dataset: Indian_pines

https://www.openml.org/d/41972

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Manvel Khudinyan  
**Source**: [original](https://purr.purdue.edu/publications/1947/1) - The imagery was collected on 12 June 1992 and represents a 2.9 by 2.9 km area  in Tippecanoe County, Indiana, USA  
**Please cite**:   

Data contains the information of 9144 samples form 220 spectral bands. The classes represent land-use types: alfalfa, corn, grass, hay, oats, soybeans,  trees, and wheat.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41972) of an [OpenML dataset](https://www.openml.org/d/41972). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41972/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41972/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41972/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

